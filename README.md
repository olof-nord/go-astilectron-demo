# go-astilectron-demo

Demo go project for serving a fronted with Electron a go project using astilectron.

https://github.com/asticode/go-astilectron

![Screenshot of running application](screenshot.png)

## Run

To run locally, use the following:

```shell
go run .
```

## Build

To build, use the following:

```shell
go build
```
