module gitlab.com/olof-nord/go-astilectron-demo

go 1.16

require (
	github.com/asticode/go-astikit v0.18.0
	github.com/asticode/go-astilectron v0.22.4
)
